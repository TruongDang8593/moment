# MIT License

# Copyright (c) 2018 Andrew Giuliani

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

###################################################################################

# INSTRUCTIONS :

# The following code snippet reads the mesh file, identifies the interpolation points 
# and correspoding weights for a vertex neighbourhood of an element and writes this information into a .dat file

# To run the code, enter the following command in the terminal :
# python moment2D.py <inputMesh.msh> <outputfile.dat>

###################################################################################

import time
from sys import argv
import random
import copy
import math

out3 = 0
ad = [1,0]

def dot(n, m):
    return sum(  [n[i]*m[i] for i in range(len(n))]    )

def getKey(item):
    return item[1]

class Face(object):
    def __init__(self, in_type, in_left_elem, in_right_elem, in_vertices):
        self.type = in_type
        self.vertices_list = copy.deepcopy(in_vertices)  # order of vertices matters for PDE solver!
        self.vertices = frozenset(in_vertices)
        self.pos = -1
        self.left_elem = in_left_elem
        self.right_elem = in_right_elem
        self.lsn = 0
        self.rsn = 0
        self.color = -1
        self.neigh_faces = None
        self.centroid_coords = None

    def set_position(self, in_num):
        self.pos = in_num


class Element(object):
    def __init__(self, in_type, in_vertices):
        self.type = in_type
        self.vertices = in_vertices
        self.pos = -1
        self.faces = []
        self.neighbors = set()
        self.d1 = [-1,-1]
        self.d2 = [-1,-1]
        self.gamma1 = [-1,-1]
        self.gamma2 = [-1,-1]
        self.interp_plus1 = [-1,-1]
        self.interp_minus1 = [-1,-1]
        self.interp_plus2 = [-1,-1]
        self.interp_minus2 = [-1,-1]
        self.alpha_plus1 = [-1,-1]
        self.alpha_minus1 = [-1,-1]
        self.alpha_plus2 = [-1,-1]
        self.alpha_minus2 = [-1,-1]
        self.neighbourhood = set()
        self.h = 0
        self.htilde = 0
        self.donotlimit = 0

    def set_position(self, in_num):
        self.pos = in_num


def get_neigh_faces(face):
    le = set(face.left_elem.faces)
    re = set(face.right_elem.faces)
    if face.right_elem.type > -1:
        neigh_faces = le.union(re).difference(set([face]))
    else:
        neigh_faces = le.difference(set([face]))

    return list(neigh_faces)

def normalize(n):
    total = math.sqrt(sum(  [n[i]*n[i] for i in range(len(n))] ))
    return [n[i]/ total for i in range(len(n))]

if __name__ == "__main__":
    inFilename = argv[1]
    outLimiterFilename = argv[2]

    inFile = open(inFilename, "rb")
    outFileLimiter = open(outLimiterFilename, "wb")

    print "? reading file: %s..." % inFilename
    line = inFile.readline()
    while "$Nodes" not in line:
        line = inFile.readline()

    # the next line is the number of vertices
    num_vertices = int(inFile.readline())
    boundary = [0]*num_vertices

    vertex_list = []
    vertex2elements = []
    for i in xrange(0, num_vertices):
        s = inFile.readline().split()
        vertex_list.append((float(s[1]), float(s[2])))

        vertex2elements.append([])
    # next two lines are just filler
    inFile.readline()
    inFile.readline()

    # next line is the number of elements and faces
    num_contents = int(inFile.readline())
    elem_list = []
    face_list = []
    face_dict = {}
    inflow = 0
    outflow = 0
    reflecting = 0
    axisym = 0

    for i in xrange(num_contents):
        s = inFile.readline().split()

        shift = 3 + int(s[2])

        if int(s[3]) == 1 or int(s[3]) == 10000:
            reflecting += 1
        elif int(s[3]) == 2 or int(s[3]) == 20000:
            outflow += 1
        elif int(s[3]) == 3 or int(s[3]) == 30000:
            inflow += 1
        elif int(s[3]) == 4 or int(s[3]) == 40000:
            axisym += 1

        # line
        if int(s[1]) == 1:
            v1 = int(s[shift + 0]) - 1
            v2 = int(s[shift + 1]) - 1
            if int(s[3]) > 4:
                face_list.append(Face(1, None, Element(-int(s[3])/10000, -1), [v1, v2]))
            else:
                face_list.append(Face(1, None, Element(-int(s[3]), -1), [v1, v2]))
            face_dict[frozenset([v1, v2])] = face_list[-1]
            boundary[v1] = 1
            boundary[v2] = 1
        # triangle.
        elif int(s[1]) == 2:
            v1 = int(s[shift + 0]) - 1
            v2 = int(s[shift + 1]) - 1
            v3 = int(s[shift + 2]) - 1


            V1x = vertex_list[v1][0]
            V1y = vertex_list[v1][1]
            V2x = vertex_list[v2][0]
            V2y = vertex_list[v2][1]
            V3x = vertex_list[v3][0]
            V3y = vertex_list[v3][1]

            # enforce strictly positive jacobian
            J = (V2x - V1x) * (V3y - V1y) - (V3x - V1x) * (V2y - V1y)
            # swap vertex 0 and 1
            if (J < 0):
                v1 , v2 = v2 , v1
                print 'Jac flipped'

            if out3:
                centroid = [(V1x+V2x+V3x)/3., (V1y+V2y+V3y)/3.]
                normals = [[-1,-1],[-1,-1],[-1,-1]]
                normals[0] = [ vertex_list[v2][1] -vertex_list[v1][1] , -(vertex_list[v2][0] -vertex_list[v1][0]) ]
                normals[1] = [ vertex_list[v3][1] -vertex_list[v2][1] , -(vertex_list[v3][0] -vertex_list[v2][0]) ]
                normals[2] = [ vertex_list[v1][1] -vertex_list[v3][1] , -(vertex_list[v1][0] -vertex_list[v3][0]) ]
                normals[0] = normalize(normals[0])
                normals[1] = normalize(normals[1])
                normals[2] = normalize(normals[2])

                v1_coord = vertex_list[v1]
                v2_coord = vertex_list[v2]
                v3_coord = vertex_list[v3]
                if dot( normals[0], [v3_coord[0] - centroid[0], v3_coord[1] - centroid[1]] ) > 0:
                    normals[0][0] = -normals[0][0]
                    normals[0][1] = -normals[0][1]
                    print 'flipped'
                if dot( normals[1], [v1_coord[0] - centroid[0], v1_coord[1] - centroid[1]] ) > 0:
                    normals[1][0] = -normals[1][0]
                    normals[1][1] = -normals[1][1]
                    print 'flipped'
                if dot( normals[2], [v2_coord[0] - centroid[0], v2_coord[1] - centroid[1]] ) > 0:
                    normals[2][0] = -normals[2][0]
                    normals[2][1] = -normals[2][1]
                    print 'flipped'

                vlist = [v1,v2,v3]
                for iteration in xrange(3):
                    if dot(normals[2], ad)> 1e-8:
                        break
                    vlist.append(vlist.pop(0))
                    normals.append(normals.pop(0))
                v1 = vlist[0]
                v2 = vlist[1]
                v3 = vlist[2]




            elem2add = Element(2, (v1, v2, v3))
            elem_list.append(elem2add)
            vertex2elements[v1].append(elem2add)
            vertex2elements[v2].append(elem2add)
            vertex2elements[v3].append(elem2add)


    inFile.close()


    num_elem = len(elem_list)
    print "  There are (%i) elements." % num_elem
    print "  There are (%i) boundary faces." % len(face_list)
    num_boundary = len(face_list)
    print "? finding interior faces."
    for elem in elem_list:
        # add the sides of each element to face_list if the faces are not in the face dictionary
        if elem.type == 2:  # triangles
            v0 = elem.vertices[0]
            v1 = elem.vertices[1]
            v2 = elem.vertices[2]

            list_of_faces = [[v0, v1], [v1, v2], [v2, v0]]
            face_type = [1] * len(list_of_faces)
        else:
            print "Error element type not found (%i)\n" % elem.type
            quit()
        for face_num, test_face in enumerate(list_of_faces):
            set_test_face = frozenset(test_face)
            if (set_test_face in face_dict):
                elem.faces.append(face_dict[set_test_face])

                if (face_dict[set_test_face].left_elem == None):
                    face_dict[set_test_face].left_elem = elem
                    face_dict[set_test_face].lsn = face_num

                elif (face_dict[set_test_face].right_elem == None):
                    face_dict[set_test_face].right_elem = elem
                    face_dict[set_test_face].rsn = face_num
                else:
                    print "This mesh has non-manifold faces i.e. more than two elements incident on a face.  Not allowed."
                    quit()

            else:
                face_list.append(Face(face_type[face_num], elem, None, test_face))
                elem.faces.append(face_list[-1])
                face_dict[set_test_face] = face_list[-1]
                face_list[-1].lsn = face_num

    for i, face in enumerate(face_list):
        # populate left and right element neighbor lists
        le = face.left_elem
        re = face.right_elem
        le.neighbors.add(re)
        if (not re == None):
            re.neighbors.add(le)
        else:
            print "  Problem with msh file: there is a boundary edge not accounted for!", i, face.vertices
            quit()
        face.neigh_faces = get_neigh_faces(face)

    for elem in elem_list:
        for neigh in elem.neighbors:
            if not elem in neigh.neighbors:
                print "connectivity problem."

    num_faces = len(face_list)

    print "  There are (%i) faces total in the mesh.\n" % num_faces

    face_types = [face.type for face in face_list]
    elem_types = [elem.type for elem in elem_list]
    print "  BC STATISTICS:\n  (%i) inflow\n  (%i) outflow\n  (%i) reflecting\n  (%i) axisymmetric\n " % (
        inflow, outflow, reflecting, axisym)
    print "  FACE STATISTICS:\n  (%i) lines\n  (%i) tri\n  (%i) quad\n " % (
        face_types.count(1), face_types.count(2), face_types.count(3))
    print "  ELEM STATISTICS:\n  (%i) tri\n  (%i) quads \n  (%i) tets \n  (%i) hexes\n  (%i) prisms\n  (%i) pyramids\n " % (
        elem_types.count(2), elem_types.count(3), elem_types.count(4), elem_types.count(5), elem_types.count(6),
        elem_types.count(7))

    for i, face in enumerate(face_list):
        face.pos = i

    # compile centroid coordinates
    centroid_coords = []
    for elem in elem_list:
        idx1 = elem.vertices[0]
        idx2 = elem.vertices[1]
        idx3 = elem.vertices[2]
        v1 = vertex_list[idx1]
        v2 = vertex_list[idx2]
        v3 = vertex_list[idx3]
        x_c = (v1[0] + v2[0] + v3[0]) / 3.
        y_c = (v1[1] + v2[1] + v3[1]) / 3.
        elem.centroid_coords = (x_c, y_c)


    for i,elem in enumerate(elem_list):
        elem.pos = i

    # populate neighbourhood
    print "? computing limiting data"

    for elem in elem_list:

        for v in elem.vertices:
            elem.neighbourhood = elem.neighbourhood.union(vertex2elements[v])
        elem.neighbourhood = elem.neighbourhood.difference(set([elem]))
        elem.ordered_neighbourhood = list(elem.neighbourhood)

        n_list = []
        for barycenter in [temp.centroid_coords for temp in elem.ordered_neighbourhood]:
            # order the neighbours
            n = [  barycenter[0] - elem.centroid_coords[0], barycenter[1] - elem.centroid_coords[1]   ]
            mag = math.sqrt(n[0]*n[0] + n[1]*n[1])
            n = [n[0]/mag, n[1]/mag]
            if n[0] > 0 and n[1] > 0:
                n_list.append( math.atan(n[1]/n[0]) )
            elif n[0] < 0 and n[1] > 0:
                n_list.append( math.atan(n[1]/n[0]) + math.pi)
            elif n[0] < 0 and n[1] < 0:
                n_list.append( math.atan(n[1]/n[0]) + math.pi )
            elif n[0] > 0 and n[1] < 0:
                n_list.append(2*math.pi + math.atan(n[1]/n[0]) )
            else:
                if -1e-10 < n[1] and  n[1] < 1e-10 and n[0] > 0:
                    n_list.append(0)
                elif -1e-10 < n[0] and  n[0] < 1e-10 and n[1] > 0:
                    n_list.append(math.pi/2.)
                elif -1e-10 < n[1] and  n[1] < 1e-10 and n[0] < 0:
                    n_list.append(math.pi)
                else:
                    n_list.append(3.*math.pi/2.)



        sorted_list = sorted([ [elem.ordered_neighbourhood[i], n_list[i]] for i in xrange(len(elem.neighbourhood))], key=getKey)
        elem.ordered_neighbourhood = [sorted_list[i][0] for i in xrange(len(elem.neighbourhood))]


    n1 = [0,0]
    n2 = [0,0]
    canonicaln1 = [2 / math.sqrt(5) * 1, 2 / math.sqrt(5) * -1. / 2.]
    canonicaln2 = [0, 1.]


    for elem in elem_list:
        idx1 = elem.vertices[0]
        idx2 = elem.vertices[1]
        idx3 = elem.vertices[2]
        v1 = vertex_list[idx1]
        v2 = vertex_list[idx2]
        v3 = vertex_list[idx3]

        c = []
        for neigh in elem.ordered_neighbourhood:
            c.append(neigh.centroid_coords)
        centroid = elem.centroid_coords

        l3 =      math.sqrt( (v3[0] - v1[0])*(v3[0] - v1[0]) + (v3[1] - v1[1])*(v3[1] - v1[1])  )
        l_tilde = math.sqrt( (v2[0] - 0.5*(v1[0] + v3[0]) )*(v2[0] - 0.5*(v1[0] + v3[0])) + (v2[1] - 0.5*(v1[1] + v3[1]))*(v2[1] - 0.5*(v1[1] + v3[1]))  )

        xr = v2[0] - v1[0]
        xs = v3[0] - v1[0]
        yr = v2[1] - v1[1]
        ys = v3[1] - v1[1]

        n1[0] = xr*canonicaln1[0] + xs * canonicaln1[1]
        n1[1] = yr*canonicaln1[0] + ys * canonicaln1[1]
        n2[0] = xr*canonicaln2[0] + xs * canonicaln2[1]
        n2[1] = yr*canonicaln2[0] + ys * canonicaln2[1]
        n1 = normalize(n1)
        n2 = normalize(n2)
        
        
        line1_x = [elem.centroid_coords[0]]
        line1_y = [elem.centroid_coords[1]]
        line2_x = [elem.centroid_coords[0]]
        line2_y = [elem.centroid_coords[1]]
        line3_x = [elem.centroid_coords[0]]
        line3_y = [elem.centroid_coords[1]]
        line4_x = [elem.centroid_coords[0]]
        line4_y = [elem.centroid_coords[1]]
        for idx in xrange(len(c)):
            v1 = n1
            # line 1 a1*x+b1*y + c1 = 0
            dx = c[(idx + 1) % len(c)][0] - c[idx][0]
            dy = c[(idx + 1) % len(c)][1] - c[idx][1]
            a1 = -dy
            b1 = dx
            c1 = c[idx][0]*dy - c[idx][1]*dx

            # line 2 a2*x+b2*y + c2 = 0
            dx = v1[0]
            dy = v1[1]
            a2 = -dy
            b2 = dx
            c2 = centroid[0]*dy - centroid[1]*dx

            det = a1*b2-b1*a2
            if(abs(det) > 1e-10) : #the uh oh parallel
                x_i = ( b2*(-c1) + -b1*(-c2))/det
                y_i = (-a2*(-c1) +  a1*(-c2))/det

                dist1 = math.sqrt( (c[idx][0]-x_i)*(c[idx][0]-x_i) + (c[idx][1]-y_i)*(c[idx][1]-y_i)    )
                dist2 = math.sqrt( (c[(idx + 1) % len(c)][0]-x_i)*(c[(idx + 1) % len(c)][0]-x_i) + (c[(idx + 1) % len(c)][1]-y_i)*(c[(idx + 1) % len(c)][1]-y_i)    )
                dist3 = math.sqrt( (c[(idx + 1) % len(c)][0]-c[idx][0])*(c[(idx + 1) % len(c)][0]-c[idx][0]) + (c[(idx + 1) % len(c)][1]-c[idx][1])*(c[(idx + 1) % len(c)][1]-c[idx][1])    )
                dist =  math.sqrt( (x_i-centroid[0])*(x_i-centroid[0]) + (y_i-centroid[1])*(y_i-centroid[1]))

                if dist1+dist2 < dist3+1e-10 and 3*dist/l_tilde > 1.-1e-10:
                    if (x_i-centroid[0])*v1[0] + (y_i-centroid[1])*v1[1] > 0:
                        if 3*dist/l_tilde > elem.gamma1[0]:
                            elem.gamma1[0] = 3*dist/l_tilde
                            elem.interp_plus1[0] = elem.ordered_neighbourhood[idx]
                            elem.interp_plus1[1] = elem.ordered_neighbourhood[(idx+1)%len(c)]
                            elem.alpha_plus1[0] = dist2 / (dist1 + dist2)
                            elem.alpha_plus1[1] = dist1 / (dist1 + dist2)
                            line1_x.append(x_i)
                            line1_y.append(y_i)
                    else:
                        if 3*dist/l_tilde > elem.gamma1[1]:
                            elem.gamma1[1] = 3*dist/l_tilde
                            elem.interp_minus1[0] = elem.ordered_neighbourhood[idx]
                            elem.interp_minus1[1] = elem.ordered_neighbourhood[(idx+1)%len(c)]
                            elem.alpha_minus1[0] = dist2 / (dist1 + dist2)
                            elem.alpha_minus1[1] = dist1 / (dist1 + dist2)
                            line2_x.append(x_i)
                            line2_y.append(y_i)

            v1 = n2
            dx = c[(idx + 1) % len(c)][0] - c[idx][0]
            dy = c[(idx + 1) % len(c)][1] - c[idx][1]
            a1 = -dy
            b1 = dx
            c1 = c[idx][0]*dy - c[idx][1]*dx

            # line 2 a2*x+b2*y + c2 = 0
            dx = v1[0]
            dy = v1[1]
            a2 = -dy
            b2 = dx
            c2 = centroid[0]*dy - centroid[1]*dx
            det = a1*b2-b1*a2
            if(abs(det) > 1e-10) : #the uh oh parallel
                x_i = ( b2*(-c1) + -b1*(-c2))/det
                y_i = (-a2*(-c1) +  a1*(-c2))/det

                dist1 = math.sqrt( (c[idx][0]-x_i)*(c[idx][0]-x_i) + (c[idx][1]-y_i)*(c[idx][1]-y_i)    )
                dist2 = math.sqrt( (c[(idx + 1) % len(c)][0]-x_i)*(c[(idx + 1) % len(c)][0]-x_i) + (c[(idx + 1) % len(c)][1]-y_i)*(c[(idx + 1) % len(c)][1]-y_i)    )
                dist3 = math.sqrt( (c[(idx + 1) % len(c)][0]-c[idx][0])*(c[(idx + 1) % len(c)][0]-c[idx][0]) + (c[(idx + 1) % len(c)][1]-c[idx][1])*(c[(idx + 1) % len(c)][1]-c[idx][1])    )
                dist =  math.sqrt( (x_i-centroid[0])*(x_i-centroid[0]) + (y_i-centroid[1])*(y_i-centroid[1]))

                if dist1+dist2 < dist3 +1e-10 and 2*dist/l3 > 1.-1e-10:
                    if (x_i-centroid[0])*v1[0] + (y_i-centroid[1])*v1[1] > 0:
                        if 2*dist/l3 > elem.gamma2[0]:
                            elem.gamma2[0] = 2*dist/l3
                            elem.interp_plus2[0] = elem.ordered_neighbourhood[idx]
                            elem.interp_plus2[1] = elem.ordered_neighbourhood[(idx+1)%len(c)]
                            elem.alpha_plus2[0] = dist2 / (dist1 + dist2)
                            elem.alpha_plus2[1] = dist1 / (dist1 + dist2)
                            line3_x.append(x_i)
                            line3_y.append(y_i)
                    else:
                        if 2*dist/l3 > elem.gamma2[1]:
                            elem.gamma2[1] = 2*dist/l3
                            elem.interp_minus2[0] = elem.ordered_neighbourhood[idx]
                            elem.interp_minus2[1] = elem.ordered_neighbourhood[(idx+1)%len(c)]
                            elem.alpha_minus2[0] = dist2 / (dist1 + dist2)
                            elem.alpha_minus2[1] = dist1 / (dist1 + dist2)
                            line4_x.append(x_i)
                            line4_y.append(y_i)


    # compute geometrical paramters for CFL
    for elem in elem_list:
        v1 = vertex_list[elem.vertices[0]]
        v2 = vertex_list[elem.vertices[1]]
        v3 = vertex_list[elem.vertices[2]]
        l1 = math.sqrt(  (v1[0]-v2[0])*(v1[0]-v2[0]) + (v1[1]-v2[1])*(v1[1]-v2[1])   )
        l2 = math.sqrt(  (v2[0]-v3[0])*(v2[0]-v3[0]) + (v2[1]-v3[1])*(v2[1]-v3[1])   )
        l3 = math.sqrt(  (v3[0]-v1[0])*(v3[0]-v1[0]) + (v3[1]-v1[1])*(v3[1]-v1[1])   )
        k = 0.5*(l1 + l2 + l3)
        area = math.sqrt(k*(k-l1)*(k-l2)*(k-l3))
        elem.h = area/k
        elem.htilde = min([area / l1, area / l2, area / l3])

    # write the mesh to file
    print "? writing file: %s..." % "limiter.dat"
    for elem in elem_list:
        no = 0
        if elem.gamma1[0] >= 1.-1e-10:
            interp_plus1 = [str(t.pos) for t in elem.interp_plus1]
            alpha_plus1 = [str(t) for t in elem.alpha_plus1]
            interp_plus1 = " ".join(interp_plus1)
            alpha_plus1 = " ".join(alpha_plus1)
            outFileLimiter.write("%s " % (interp_plus1 + " " + alpha_plus1))
        else:
            outFileLimiter.write("-1 -1 -1 -1 ")
            no = 1

        if elem.gamma2[0] >= 1.-1e-10:
            interp_plus2 = [str(t.pos) for t in elem.interp_plus2]
            alpha_plus2 = [str(t) for t in elem.alpha_plus2]
            interp_plus2 = " ".join(interp_plus2)
            alpha_plus2 = " ".join(alpha_plus2)
            outFileLimiter.write("%s " % (interp_plus2 + " " + alpha_plus2))
        else:
            outFileLimiter.write("-1 -1 -1 -1 ")
            no = 1

        if elem.gamma1[1] >= 1.-1e-10:
            interp_minus1 = [str(t.pos) for t in elem.interp_minus1]
            alpha_minus1 = [str(t) for t in elem.alpha_minus1]
            interp_minus1 = " ".join(interp_minus1)
            alpha_minus1 = " ".join(alpha_minus1)
            outFileLimiter.write("%s " % (interp_minus1 + " " + alpha_minus1))
        else:
            outFileLimiter.write("-1 -1 -1 -1 ")
            no = 1

        if elem.gamma2[1] >= 1.-1e-10:
            interp_minus2 = [str(t.pos) for t in elem.interp_minus2]
            alpha_minus2 = [str(t) for t in elem.alpha_minus2]
            interp_minus2 = " ".join(interp_minus2)
            alpha_minus2 = " ".join(alpha_minus2)
            outFileLimiter.write("%s " % (interp_minus2 + " " + alpha_minus2))
        else:
            outFileLimiter.write("-1 -1 -1 -1 ")
            no = 1

        if no:
            v1 = vertex_list[elem.vertices[0]]
            v2 = vertex_list[elem.vertices[1]]
            v3 = vertex_list[elem.vertices[2]]
            x_coords = [v1[0], v2[0], v3[0],v1[0]]
            y_coords = [v1[1], v2[1], v3[1],v1[1]]
        gamma1 = [ str(t) for t in elem.gamma1 ]
        gamma1 = " ".join(gamma1)
        gamma2 = [ str(t) for t in elem.gamma2 ]
        gamma2 = " ".join(gamma2)

        d1 = [ str(t) for t in elem.d1 ]
        d1 = " ".join(d1)
        d2 = [ str(t) for t in elem.d2 ]
        d2 = " ".join(d2)
        outFileLimiter.write("%s\n" % (gamma1 + " " + gamma2 + " " + d1 +" " + d2 + " " + str(elem.htilde)))

    
