#include <stdio.h>
#include <time.h>
#include "math.h"
#include "uthash.h"

#define qh_QHimport
#include "libqhull_r/qhull_ra.h"
//#include "libqhull_r/qhull_ra.h"
#define DIM 3
#define MAX_NEIGH 150





void print_summary(qhT *qh) {
  facetT *facet;
  vertexT *vertex, **vertexp;

  printf("\n%d vertices and %d facets\n",
                 qh->num_vertices, qh->num_facets);
  FORALLfacets {
        printf ("%d", qh_setsize (qh, facet->vertices) );
          FOREACHvertex_(facet->vertices)
            printf (" %d", qh_pointid (qh, vertex->point));
    printf("\n");
  }
}

double dot(double *a, double *b) {return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];}

void diff(double *out, double *a, double *b){
    out[0] = a[0]-b[0];
    out[1] = a[1]-b[1];
    out[2] = a[2]-b[2];
}
void cross(double *out, double *a, double *b){
    out[0] =   a[1]*b[2] - a[2]*b[1];
    out[1] = -(a[0]*b[2] - a[2]*b[0]);
    out[2] =   a[0]*b[1] - a[1]*b[0];
}

double mag(double *a) {return sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);}

void compute_weights(double *w1, double *w2, double *w3,
                     double *x1, double *x2, double *x3, double *p){
  double A1, A2, A3, A;
  double a[3], b[3], c[3]; 

  diff(a, x2, x1);diff(b, x3, x1);cross(c, a, b); A  = mag(c)/2.;
  diff(a, x1, p); diff(b, x2, p); cross(c, a, b); A3 = mag(c)/2.;
  diff(a, x2, p); diff(b, x3, p); cross(c, a, b); A1 = mag(c)/2.;
  diff(a, x3, p); diff(b, x1, p); cross(c, a, b); A2 = mag(c)/2.;

  *w1 = A1/A;
  *w2 = A2/A;
  *w3 = A3/A;
}

void normalize(double *a, double mag){
    a[0]/=mag;
    a[1]/=mag;
    a[2]/=mag;
}

// int inside(double *x1, double *x2, double *x3, double *p){
//     double len1, len2, len3, dot1, dot2, dot3;
//     double v1[3], v2[3], v3[3];

//     diff(v1, x1, p); diff(v2, x2, p); diff(v3, x3, p);
//     len1 = mag(v1); if(len1>1e-10) normalize(v1, len1);
//     len2 = mag(v2); if(len2>1e-10) normalize(v2, len2);
//     len3 = mag(v3); if(len3>1e-10) normalize(v3, len3);

//     dot1 = dot(v1, v2);
//     dot2 = dot(v1, v3);
//     dot3 = dot(v2, v3);


//     // printf("angles %lf %lf %lf\n", (len1 > 1e-10)*acos(dot1), (len2 > 1e-10)*acos(dot2), (len3 > 1e-10)*acos(dot3));
//     return (len1 > 1e-10)*acos(dot1) + (len2 > 1e-10)*acos(dot2) + (len3 > 1e-10)*acos(dot3) < 2 * M_PI +1e-10;
// }

void multiply(double *out, double A[3][3], double *b){
    out[0] = A[0][0]*b[0] + A[0][1]*b[1] + A[0][2]*b[2];
    out[1] = A[1][0]*b[0] + A[1][1]*b[1] + A[1][2]*b[2];
    out[2] = A[2][0]*b[0] + A[2][1]*b[1] + A[2][2]*b[2];
}

struct edge;
struct Element;

typedef struct element
{
    int type;
    int nodes[4];
    int pos;
    int concave;
    double centroid[3];
    struct edge *sides[4];
    struct element *neighbors[4];
    int num_vertex_neigh;
    struct element *vertex_neighbors[MAX_NEIGH];

    // int e_f[3][3];
    // int e_b[3][3];
    // double w_f[3][3];
    // double w_b[3][3];

} Element;


typedef struct edge
{
    char name[100];
    int nodes[3];
    int pos;
    int boundary;
    int color;
    int lsn;
    int rsn;
    int orient;
    struct element *le, *re;
    struct edge *adj[6];

    int visited;
    UT_hash_handle hh;
} Edge;

typedef struct Node
{
    int boundary;
    double coords[3];
} Node;



void get_id(char *id, int n1, int n2, int n3){
    sprintf(id, "%i %i %i", n1, n2, n3);
}

int indexof(Element *t, Element **list, int size){
    for(int i = 0; i < size; i++){
        if( t == list[i])
            return i;
    }
    return -1;
}

void add(Element *t, Element **list, int *size){
    list[*size] = t;
    *size = *size + 1 ;
}



Edge *edge_hash = NULL;
Edge *q_edge_hash = NULL;

Edge* edge_insert(int n1, int n2, int n3, int sn, Element *in_tet, Edge **edge_list, int *edge_count){
    char id[6][100];
    int int_id[6];
    
    get_id(id[0], n1 , n2 , n3);
    get_id(id[1], n3 , n2 , n1);
    
    get_id(id[2], n3 , n1 , n2);
    get_id(id[3], n2 , n1 , n3);
    
    get_id(id[4], n2 , n3 , n1);
    get_id(id[5], n1 , n3 , n2);

    int_id[0] = n1;
    int_id[1] = n3;
    int_id[2] = n3;
    int_id[3] = n2;
    int_id[4] = n2;
    int_id[5] = n1;

    Edge *exists = NULL;
    for(int i = 0; i < 6; i++){
        HASH_FIND_STR( edge_hash, id[i], exists);
        if(exists){
            if(exists->boundary){
                exists->le = in_tet;
                exists->lsn = sn;
            }
            else
                exists->re = in_tet;
                exists->rsn = sn;
                if(int_id[i] == n1)
                    exists->orient = 0;
                else if(int_id[i] == n2)
                    exists->orient = 1;
                else
                    exists->orient = 2;
            break;
        }
    }
    if(!exists){
        exists = (Edge *) malloc(sizeof(Edge));
        sprintf(exists->name, "%i %i %i", n1, n2, n3);
        exists->nodes[0] = n1;
        exists->nodes[1] = n2;
        exists->nodes[2] = n3;
        exists->le = in_tet;
        exists->re = NULL;
        exists->lsn = sn;
        exists->rsn = -1;
        exists->orient = -1;
        exists->boundary = 0;
        exists->color = 0;
        edge_list[*edge_count] = exists;
        HASH_ADD_STR( edge_hash, name,  exists);
        *edge_count = *edge_count + 1;
    }
    
    return exists;
}



void populate_hash(int *color_hash, Edge *p_edge){
    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    Element *re = p_edge->re;
    color_hash[p_edge->adj[0]->color]++;
    color_hash[p_edge->adj[1]->color]++;
    color_hash[p_edge->adj[2]->color]++;
    if(re){
        color_hash[p_edge->adj[3]->color]++;
        color_hash[p_edge->adj[4]->color]++;
        color_hash[p_edge->adj[5]->color]++;
    }
}

void find(Edge *p_edge, int *d1, int* d2, int *d3, int* d4){
    int color_list[] = {0,0,0,0,0,0};
    Element *re = p_edge->re;
    color_list[0] = p_edge->adj[0]->color;
    color_list[1] = p_edge->adj[1]->color;
    color_list[2] = p_edge->adj[2]->color;
    if(re){
        color_list[3]= p_edge->adj[3]->color;
        color_list[4]= p_edge->adj[4]->color;
        color_list[5]= p_edge->adj[5]->color;
    }
    
    *d1 = -1; *d2 = -1; *d3 = -1; *d4 = -1;
//    printf("colorlist %i %i %i %i %i %i\n", color_list[0], color_list[1], color_list[2], color_list[3], color_list[4], color_list[5]);
    for(int i = 0; i < 6; i++){
        for(int j = i; j < 6; j++){
            if( (color_list[i] == color_list[j]) && (color_list[i]!=0) &&(color_list[j]!=0) && (i!=j)){
                if(*d1 == -1){
                    *d1 = i; *d2 = j;
                }
                else{
                    *d3 = i; *d4 = j;
                    break;
                }
            }
        }
    }
    
}

int resolve(Edge *p_edge, int color_hash[5]){
    int pos = 1;
    while(color_hash[pos] > 0 && pos < 5)
        pos++;
    
    if(1 <= pos && pos <=4)
        return pos;
    else
        return 0;
}

Edge* swap_with(Edge *p_edge, int conflict_ID, int d1, int d2, int d3, int d4){
    int swap_list[] = {1,1,1,0,0,0};
    int swap_ID[] = {-1,-1,-1,-1,-1,-1};
    int swap_count = 0;
    Element *re = p_edge->re;
    
    if(re){
        swap_list[3] = 1;
        swap_list[4] = 1;
        swap_list[5] = 1;
    }
    
    if(d1 > -1 && d2 > -1){
        swap_list[d1] = 0;
        swap_list[d2] = 0;
    }
    if(d3 > -1 && d4 > -1){
        swap_list[d3] = 0;
        swap_list[d4] = 0;
    }
    
    for(int i = 0; i < 6; i++){
        if(swap_list[i] && p_edge->adj[i]->visited != conflict_ID && p_edge->adj[i]->color != 0){
            swap_ID[swap_count] = i;
            swap_count++;
        }
    }
    
    if(swap_count>0){
        return p_edge->adj[swap_ID[rand()%swap_count]];
    }
    else
        return NULL;
}


int main(int argc, char * argv[]) {
    time_t t;
    srand((unsigned) time(&t));
    int node_count=0, tet_count=0, boundary_count=0, edge_count=0, size;
    char line[100];
    FILE * inFileP = fopen(argv[1], "r");
    fgets(line, sizeof line, inFileP);
    while(strncmp(line,"$Nodes", 6)!=0)
        fgets(line, sizeof line, inFileP);
    fscanf(inFileP, "%i", &node_count);
    
    printf("node_count is %i\n", node_count);
    Node *node_list = (Node *) malloc( node_count * sizeof(Node));
    
    
    for(int n = 0; n < node_count; n++){
        fscanf(inFileP, "%*i %lf %lf %lf", node_list[n].coords + 0, node_list[n].coords + 1, node_list[n].coords + 2);
        node_list[n].boundary = 0;
    }
    
    fgets(line, sizeof line, inFileP);
    while(strncmp(line,"$Elements", 8)!=0)
        fgets(line, sizeof line, inFileP);
    fgets(line, sizeof line, inFileP) ;
    sscanf(line, "%i", &size);
    
    
    Edge **boundary_list = (Edge **) malloc( size * sizeof(Edge*));
    Edge **edge_list = (Edge **) malloc( 4*size * sizeof(Edge*));
    Element **tet_list = (Element **) malloc( size * sizeof(Element*));
    int type;
    
    Edge *temp;
    Edge *exists;
    
    double detJ ;
    double xr, xs, xt, yr, ys, yt, zr, zs, zt;
    double V1x, V1y, V1z;
	double V2x, V2y, V2z;
	double V3x, V3y, V3z;
	double V4x, V4y, V4z;
	int nn1, nn2, nn3, nn4;

    for(int n = 0; n < size; n++){
        type = -1;
        fgets(line, sizeof line, inFileP) ;
        sscanf(line, "%*i %i", &type);
        if(type == 2){
            
            temp = (Edge *) malloc(sizeof(Edge));
            sscanf(line, "%*i %*i %*i %*i %*i %i %i %i", temp->nodes + 0, temp->nodes + 1, temp->nodes + 2);
            //            printf("%i %i %i\n", temp->nodes[0], temp->nodes[1], temp->nodes[2]);
            temp->nodes[0]--;
            temp->nodes[1]--;
            temp->nodes[2]--;
            temp->le = NULL;
            temp->re = NULL;
            temp->rsn = -1;
            temp->boundary = 1;
            temp->color = 0;
            
            node_list[temp->nodes[0]].boundary = 1;
            node_list[temp->nodes[1]].boundary = 1;
            node_list[temp->nodes[2]].boundary = 1;
            
            sprintf(temp->name, "%i %i %i", temp->nodes[0], temp->nodes[1], temp->nodes[2]);
            HASH_FIND_STR( edge_hash, temp->name, exists);
            
            if(!exists){
                HASH_ADD_STR( edge_hash, name,  temp);
                boundary_list[boundary_count] = temp;
                edge_list[edge_count] = temp;
                edge_count++;
                boundary_count++;
            }
            else
                free(temp);
            
        }
        else if(type == 4){
            tet_list[tet_count] = (Element *) malloc(sizeof(Element));
            sscanf(line, "%*i %*i %*i %*i %*i %i %i %i %i", tet_list[tet_count]->nodes + 0, tet_list[tet_count]->nodes + 1, tet_list[tet_count]->nodes + 2, tet_list[tet_count]->nodes + 3);
            tet_list[tet_count]->type = 2;
            tet_list[tet_count]->nodes[0]--;
            tet_list[tet_count]->nodes[1]--;
            tet_list[tet_count]->nodes[2]--;
            tet_list[tet_count]->nodes[3]--;

            nn1 = tet_list[tet_count]->nodes[0];
			nn2 = tet_list[tet_count]->nodes[1];
			nn3 = tet_list[tet_count]->nodes[2];
			nn4 = tet_list[tet_count]->nodes[3];

            V1x = node_list[nn1].coords[0] ; V1y = node_list[nn1].coords[1]; V1z = node_list[nn1].coords[2];
			V2x = node_list[nn2].coords[0] ; V2y = node_list[nn2].coords[1]; V2z = node_list[nn2].coords[2];
			V3x = node_list[nn3].coords[0] ; V3y = node_list[nn3].coords[1]; V3z = node_list[nn3].coords[2];
			V4x = node_list[nn4].coords[0] ; V4y = node_list[nn4].coords[1]; V4z = node_list[nn4].coords[2];

            xr = V2x - V1x;
	        yr = V2y - V1y;
	        zr = V2z - V1z;
	        xs = V3x - V1x;
	        ys = V3y - V1y;
	        zs = V3z - V1z;
	        xt = V4x - V1x;
	        yt = V4y - V1y;
	        zt = V4z - V1z;
            detJ = xr * (ys * zt - yt * zs) - xs * (yr * zt - yt * zr) + xt * ( yr * zs -ys * zr);

            if(detJ < 0){
            	// printf("NEGATIVE DETERMINANT OF THE JACOBIAN\n");
				tet_list[tet_count]->nodes[0] = nn2;
				tet_list[tet_count]->nodes[1] = nn1;
				// nn1 = tet_list[tet_count]->nodes[0];
				// nn2 = tet_list[tet_count]->nodes[1];
				// nn3 = tet_list[tet_count]->nodes[2];
				// nn4 = tet_list[tet_count]->nodes[3];

				// V1x = node_list[nn1].coords[0] ; V1y = node_list[nn1].coords[1]; V1z = node_list[nn1].coords[2];
				// V2x = node_list[nn2].coords[0] ; V2y = node_list[nn2].coords[1]; V2z = node_list[nn2].coords[2];
				// V3x = node_list[nn3].coords[0] ; V3y = node_list[nn3].coords[1]; V3z = node_list[nn3].coords[2];
				// V4x = node_list[nn4].coords[0] ; V4y = node_list[nn4].coords[1]; V4z = node_list[nn4].coords[2];

				// xr = V2x - V1x;
				// yr = V2y - V1y;
				// zr = V2z - V1z;
				// xs = V3x - V1x;
				// ys = V3y - V1y;
				// zs = V3z - V1z;
				// xt = V4x - V1x;
				// yt = V4y - V1y;
				// zt = V4z - V1z;
				// detJ = xr * (ys * zt - yt * zs) - xs * (yr * zt - yt * zr) + xt * ( yr * zs -ys * zr);
				// printf("after swap %lf\n", detJ);
            }

            tet_count++;
        }
        else{
            printf("unrecognized\n");
        }
    }
    
    int n1, n2, n3;
    //initialize faces
    for(int tet = 0; tet < tet_count; tet++){
        // add side 0
        n1 = tet_list[tet]->nodes[1];
        n2 = tet_list[tet]->nodes[2];
        n3 = tet_list[tet]->nodes[3];
        tet_list[tet]->sides[0] = edge_insert(n1, n2, n3, 0, tet_list[tet], edge_list, &edge_count);
        
        // add side 1
        n1 = tet_list[tet]->nodes[2];
        n2 = tet_list[tet]->nodes[0];
        n3 = tet_list[tet]->nodes[3];
        tet_list[tet]->sides[1] = edge_insert(n1, n2, n3, 1, tet_list[tet], edge_list, &edge_count);


        // add side 2
        n1 = tet_list[tet]->nodes[0];
        n2 = tet_list[tet]->nodes[1];
        n3 = tet_list[tet]->nodes[3];
        tet_list[tet]->sides[2] = edge_insert(n1, n2, n3, 2, tet_list[tet], edge_list, &edge_count);

        // add side 3
        n1 = tet_list[tet]->nodes[2];
        n2 = tet_list[tet]->nodes[1];
        n3 = tet_list[tet]->nodes[0];
        tet_list[tet]->sides[3] = edge_insert(n1, n2, n3, 3, tet_list[tet], edge_list, &edge_count);     

    }


    
    printf("element count %i\n", size);
    printf("boundary_count %i\n", boundary_count);
    printf("tet_count %i\n", tet_count);
    printf("edge_count %i\n", edge_count);
    printf("Found all the faces!\n");
    //found all the faces
    
    // initialize neighbors.
    Element *curr_elem;
    Edge *curr_edge;
    for(int i=0; i < tet_count; i++){
        curr_elem = tet_list[i];
        for(int s = 0; s < 4; s++) {
            curr_edge = curr_elem->sides[s];
            curr_elem->neighbors[s] = (curr_edge->re == curr_elem) ? curr_edge->le : curr_edge->re;
        }
    }



    
    for (int i = 0; i < tet_count; i++)
        tet_list[i]->pos = i;
    for (int i = 0; i < edge_count; i++)
        edge_list[i]->pos = i;
    
    

    printf("Coloring!\n");

    // build adjacency lists
    Edge *p_edge;
    Element *le, *re;
    for(int e = 0; e < edge_count; e++){
        p_edge = edge_list[e];
        le = p_edge->le;
        re = p_edge->re;

         p_edge->adj[0] = le->sides[0] == p_edge ? le->sides[3]: le->sides[0];
         p_edge->adj[1] = le->sides[1] == p_edge ? le->sides[3]: le->sides[1];
         p_edge->adj[2] = le->sides[2] == p_edge ? le->sides[3]: le->sides[2];

         if(re){
             p_edge->adj[3] = re->sides[0] == p_edge ? re->sides[3]: re->sides[0];
             p_edge->adj[4] = re->sides[1] == p_edge ? re->sides[3]: re->sides[1];
             p_edge->adj[5] = re->sides[2] == p_edge ? re->sides[3]: re->sides[2];
         }
         else{
             p_edge->adj[3] = NULL;
             p_edge->adj[4] = NULL;
             p_edge->adj[5] = NULL;
         }
    }


    Edge **conflict_list = (Edge **) malloc(edge_count * sizeof(Edge*));

    // initial greedy coloring
    int color_hash[] = {0,0,0,0,0};
    int pos;
    int conflict_size = 0;
    for(int e = 0; e < edge_count; e++){
        conflict_list[conflict_size] = NULL;

        p_edge = edge_list[e];
        populate_hash(color_hash, p_edge);

        pos = -1;
        for(int i = 1; i <= 4; i++){
            if(color_hash[i] == 0 ){
                pos = i;
                break;
            }
        }

        if(1 <= pos && pos <=4)
            p_edge->color=pos;
        else{
            p_edge->color=0;
            conflict_list[conflict_size] = p_edge;
            conflict_size++;
        }
        edge_list[e]->visited = -1;
    }


    printf("There are %i conflicts\n", conflict_size);
    Edge *curr_conflict;
    int r_color, d1, d2, d3, d4;
    
    Edge *swap;
    int conflict_ID = 0;
    int iteration = 0;
    // conflict resolution
    while(conflict_size > 0){
        curr_conflict = conflict_list[conflict_size-1];
        curr_conflict->visited = conflict_ID;
        
        populate_hash(color_hash, curr_conflict);
        r_color = resolve(curr_conflict, color_hash);
        if(r_color){ // can it be resolved?
            curr_conflict->color = r_color;
            conflict_size--;
            conflict_ID++;
            continue;
        }
        
        find(curr_conflict, &d1, &d2, &d3, &d4);
        swap = swap_with(curr_conflict, conflict_ID, d1, d2, d3, d4);
        if(swap){ // swap
            curr_conflict->color = swap->color;
            swap->color = 0;
            conflict_list[conflict_size-1] = swap;
            continue;
        }
        
        
        if(d1 < 0 || d2 < 0 || d3 < 0 || d4 < 0){
            // cannot double
            conflict_ID++;
            continue;
        }
        
        // double
        curr_conflict->color= curr_conflict->adj[d1]->color;
        curr_conflict->adj[d1]->color = 0;
        curr_conflict->adj[d2]->color = 0;
        conflict_list[conflict_size-1] = curr_conflict->adj[d1];
        conflict_list[conflict_size  ] = curr_conflict->adj[d2];
        conflict_size++;
        conflict_ID++;
        
        
        
        if(iteration%100 ==0)
            printf("\r(%i) conflicts left.", conflict_size);
        iteration++;
    }
    
    printf("\nDone coloring. %i\n", conflict_size);
    
    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    for(int e = 0; e < edge_count; e++){
        color_hash[edge_list[e]->color]++;
    }
    for(int i = 0; i < 5; i++)
        printf("color %i: %i\n", i, color_hash[i]);
    
    for(int elem=0; elem < tet_count; elem++){
        color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
        color_hash[tet_list[elem]->sides[0]->color]++;
        color_hash[tet_list[elem]->sides[1]->color]++;
        color_hash[tet_list[elem]->sides[2]->color]++;
        color_hash[tet_list[elem]->sides[3]->color]++;
        if(color_hash[1] != 1 || color_hash[2] != 1 || color_hash[3] != 1 || color_hash[4] != 1 ){
            printf("problem %i %i %i %i %i\n", color_hash[0], color_hash[1], color_hash[2], color_hash[3], color_hash[4]);
            printf("problem s1 %i s2 %i s3 %i s4 %i\n", tet_list[elem]->sides[0]->color, tet_list[elem]->sides[1]->color, tet_list[elem]->sides[2]->color, tet_list[elem]->sides[2]->color);
            exit(-1);
        }
    }



    Edge **reordered_edge = (Edge **) malloc(edge_count * sizeof(Edge*));
    printf("done interpolating\n");
    printf("Reordering based on color.\n");
    int current_ID = 0;
    for(int c = 1; c <= 4; c++){
        for(int f = 0; f < edge_count; f++){
            if(edge_list[f]->color == c){
                edge_list[f]->pos = current_ID;
                reordered_edge[current_ID] = edge_list[f];
                current_ID++;
            }
        }
    }
    free(edge_list); //its unordered
    edge_list = reordered_edge;

    












    printf("compiling node2elem\n");
    
    Element ***node2elem = (Element ***) malloc(node_count * sizeof(Element **));
    int *node2elem_size = (int *) malloc(node_count * sizeof(int));
    memset(node2elem_size, 0, node_count*sizeof(int));

    for(int i = 0; i < node_count; i++)
        node2elem[i] = (Element **) malloc(MAX_NEIGH * sizeof(Element *));
    
    int nd;
    Element *elem;
    for(int i = 0; i < tet_count; i++){
        elem = tet_list[i];
        for(int n = 0; n < 4; n++){
            nd = elem->nodes[n];
            node2elem[nd][node2elem_size[nd]] = elem;
            node2elem_size[nd]++;
        }
    }
    
    for(int i = 0; i < tet_count; i++){
        elem = tet_list[i];
        for(int c = 0; c < 3; c++)
            elem->centroid[c] = (node_list[elem->nodes[0]].coords[c]+node_list[elem->nodes[1]].coords[c]+node_list[elem->nodes[2]].coords[c]+node_list[elem->nodes[3]].coords[c])/4. ;
    }

    int idx;
    for(int i = 0; i < tet_count; i++){
        elem = tet_list[i];
        elem->num_vertex_neigh = 0;

        for(int j = 0; j < 4; j++){
            nd = elem->nodes[j];
            for(int k = 0; k < node2elem_size[nd]; k++){
                idx = indexof(node2elem[nd][k], elem->vertex_neighbors, elem->num_vertex_neigh);
                if(idx == -1)
                    add(node2elem[nd][k], elem->vertex_neighbors, &(elem->num_vertex_neigh));

                if(elem->num_vertex_neigh >= MAX_NEIGH){
                    printf("MAX_NEIGH reached.\n");
                    exit(-1);
                }
            }
        }
    }
    


    facetT *facet;
    vertexT *vertex, **vertexp;

    coordT points[MAX_NEIGH * DIM] ;
    char flags[25];
    boolT ismalloc= False;
    sprintf (flags, "qhull Qt");
    qhT qh_qh;              
    qhT *qh= &qh_qh;

    FILE *errfile= NULL;
    FILE *outfile= NULL; 
    int k1 = 0, k2 = 0;

    printf("evaluating all the convex hulls\n");





    Element **out_list;
    int *out_pos;
    printf("writing dat file %s\n", argv[3]);
    FILE *outFile = fopen(argv[3], "w");



    int e_f[3][3];
    int e_b[3][3];
    double w_f[3][3];
    double w_b[3][3];


    int in;
    double a[3], b[3], c[3];
    int node1, node2, node3;
    double n[3];
    double x_i[3];
    double s;
    double dir[3][3] = { {3./sqrt(11.)     , - 1./sqrt(11.)    ,  - 1. /sqrt(11.)    }, 
                         {0.               ,   2./sqrt(5.)     ,  - 1./sqrt(5.)      },
                         {0.,                  0.,                  1.}
                       };
    double local_d[3];
    double J[3][3];

    double w1, w2, w3;
    double num, denom;
  	int curlong, totlong;     /* memory remaining after qh_memfreeshort */

    double h[3];
    int num_hull_faces;
    int face_list[MAX_NEIGH][3];

    printf("finding forward and backwards interpolation planes\n");
    for(int i = 0; i < tet_count; i++){
    	qh_zero(qh, errfile);

        for(int d = 0; d < 3; d++){
			e_f[d][0]=-1; e_f[d][1]=-1; e_f[d][2]=-1;
			e_b[d][0]=-1; e_b[d][1]=-1; e_b[d][2]=-1;
			w_f[d][0]=-1; w_f[d][1]=-1; w_f[d][2]=-1;
			w_b[d][0]=-1; w_b[d][1]=-1; w_b[d][2]=-1;
		}

        elem = tet_list[i];

        // convex hull
        for(int j = 0; j < elem->num_vertex_neigh; j++){
            points[ 3*j + 0 ] = elem->vertex_neighbors[j]->centroid[0];
            points[ 3*j + 1 ] = elem->vertex_neighbors[j]->centroid[1];
            points[ 3*j + 2 ] = elem->vertex_neighbors[j]->centroid[2];
        }

        qh_new_qhull(qh, DIM, elem->num_vertex_neigh, points, ismalloc, flags, outfile, errfile);
	    num_hull_faces = qh->num_facets;
	    k1 = 0 ;
	    FORALLfacets {
	        k2 = 0 ;
	          FOREACHvertex_(facet->vertices){
	            face_list[k1][k2] = qh_pointid (qh, vertex->point);
	            k2++;
	          }
	        k1++;

	        if(k1 >= MAX_NEIGH){
	            printf("MAX_NEIGH neighbors reached. (%i)\n", k1);
	            exit(-1);
	        }
	    }

	  qh_freeqhull(qh, !qh_ALL);                   /* free long memory  */
	  qh_memfreeshort(qh, &curlong, &totlong);    /* free short memory and memory allocator */
	  if (curlong || totlong)
	    fprintf(errfile, "qhull internal warning (user_eg, #1): did not free %d bytes of long memory (%d pieces)\n", totlong, curlong);





        for(int q = 0; q < 3; q++){
            J[0][q] = node_list[elem->nodes[q+1]].coords[0] - node_list[elem->nodes[0]].coords[0];
            J[1][q] = node_list[elem->nodes[q+1]].coords[1] - node_list[elem->nodes[0]].coords[1];
            J[2][q] = node_list[elem->nodes[q+1]].coords[2] - node_list[elem->nodes[0]].coords[2];  
        }

        multiply(local_d, J, dir[0]);
        h[0] = (sqrt(11.)/3.) * mag(local_d);
        multiply(local_d, J, dir[1]);
        h[1] =  (sqrt(5.)/2.) * mag(local_d);
        multiply(local_d, J, dir[2]);
        h[2] = mag(local_d);

        // if(i == 2312)
        //     printf("%lf %lf %lf \n %lf %lf %lf \n %lf %lf %lf \n %lf %lf %lf\n\n", node_list[elem->nodes[0]].coords[0], node_list[elem->nodes[0]].coords[1], node_list[elem->nodes[0]].coords[2],
        //                                                                            node_list[elem->nodes[1]].coords[0], node_list[elem->nodes[1]].coords[1], node_list[elem->nodes[1]].coords[2],
        //                                                                            node_list[elem->nodes[2]].coords[0], node_list[elem->nodes[2]].coords[1], node_list[elem->nodes[2]].coords[2],
        //                                                                            node_list[elem->nodes[3]].coords[0], node_list[elem->nodes[3]].coords[1], node_list[elem->nodes[3]].coords[2]);
        
        for(int f = 0; f < num_hull_faces; f++){
            node1 = face_list[f][0];
            node2 = face_list[f][1];
            node3 = face_list[f][2];


            // if(i == 2312){
            //     printf("%lf %lf %f\n", elem->vertex_neighbors[node1]->centroid[0], elem->vertex_neighbors[node1]->centroid[1], elem->vertex_neighbors[node1]->centroid[2]);
            //     printf("%lf %lf %f\n", elem->vertex_neighbors[node2]->centroid[0], elem->vertex_neighbors[node2]->centroid[1], elem->vertex_neighbors[node2]->centroid[2]);
            //     printf("%lf %lf %f\n", elem->vertex_neighbors[node3]->centroid[0], elem->vertex_neighbors[node3]->centroid[1], elem->vertex_neighbors[node3]->centroid[2]);
            // }

            diff(a, elem->vertex_neighbors[node2]->centroid, elem->vertex_neighbors[node1]->centroid);
            diff(b, elem->vertex_neighbors[node3]->centroid, elem->vertex_neighbors[node1]->centroid);
            cross(n, a, b);



            for(int d = 0; d < 3; d++){
                in = 0;

                multiply(local_d, J, dir[d]);normalize(local_d, mag(local_d));
                diff(c, elem->vertex_neighbors[node1]->centroid,elem->centroid);
                num = dot(c, n);
                denom = dot(local_d, n);
                s = 0;

                if(fabs(denom) > 1e-10){
                    s = num/denom;
                    x_i[0] = elem->centroid[0] + s * local_d[0];
                    x_i[1] = elem->centroid[1] + s * local_d[1];
                    x_i[2] = elem->centroid[2] + s * local_d[2];

                    compute_weights(&w1, &w2, &w3,
                                    elem->vertex_neighbors[node1]->centroid, 
                                    elem->vertex_neighbors[node2]->centroid, 
                                    elem->vertex_neighbors[node3]->centroid, 
                                    x_i);
                    in = (-1e-10 < w1 && w1 < 1. + 1e-10) && (-1e-10 < w2 && w2 < 1. + 1e-10) && (-1e-10 < w3 && w3 < 1. + 1e-10) && (1.- 1e-10 < w1+w2+w3 &&  w1+w2+w3 < 1. + 1e-10);
                

                }

                // if((4 - d) * fabs(s)/h[d] < 1)
                //     printf("%i %lf %lf\n", d, fabs(s)/h[d], (4 - d) * fabs(s)/h[d]);
                //     continue; // too close
                if(in && (4 - d) * fabs(s)/h[d] > 1){
                    // if(i == 2312)
                    //     printf("%i %lf\n", d, s);

                    if(s > 0) {
                        e_f[d][0] = node1;
                        e_f[d][1] = node2;
                        e_f[d][2] = node3;
                        w_f[d][0] = w1;
                        w_f[d][1] = w2;
                        w_f[d][2] = w3;
                    }
                    else {
                        e_b[d][0] = node1;
                        e_b[d][1] = node2;
                        e_b[d][2] = node3;
                        w_b[d][0] = w1;
                        w_b[d][1] = w2;
                        w_b[d][2] = w3;
                    }

                }

            }
        }


        for(int d = 0; d < 3; d++){
            if(e_f[d][0] > -1){
                out_list = tet_list[i]->vertex_neighbors;
                out_pos = e_f[d];
                fprintf(outFile, "%i %i %i %.015lf %.015lf %.015lf ", out_list[out_pos[0]]->pos,out_list[out_pos[1]]->pos,out_list[out_pos[2]]->pos, w_f[d][0], w_f[d][1], w_f[d][2]);
            }
            else{
                fprintf(outFile, "-1 -1 -1 -1 -1 -1 ");
                if(!node_list[elem->nodes[0]].boundary && !node_list[elem->nodes[1]].boundary && !node_list[elem->nodes[2]].boundary && !node_list[elem->nodes[3]].boundary)
                    printf("not on boundary...position %i, %i %i %i %i\n", elem->pos, node_list[elem->nodes[0]].boundary, node_list[elem->nodes[1]].boundary, node_list[elem->nodes[2]].boundary, node_list[elem->nodes[3]].boundary);

            }
            if(e_b[d][0] > -1){
                out_list = tet_list[i]->vertex_neighbors;
                out_pos = e_b[d];
                fprintf(outFile, "%i %i %i %.015lf %.015lf %.015lf ",out_list[out_pos[0]]->pos,out_list[out_pos[1]]->pos,out_list[out_pos[2]]->pos, w_b[d][0], w_b[d][1], w_b[d][2]);
            }
            else{
                fprintf(outFile, "-1 -1 -1 -1 -1 -1 ");
                if(!node_list[elem->nodes[0]].boundary && !node_list[elem->nodes[1]].boundary && !node_list[elem->nodes[2]].boundary && !node_list[elem->nodes[3]].boundary)
                    printf("not on boundary...position %i, %i %i %i %i\n", elem->pos, node_list[elem->nodes[0]].boundary, node_list[elem->nodes[1]].boundary, node_list[elem->nodes[2]].boundary, node_list[elem->nodes[3]].boundary);

            }
        }
        fprintf(outFile, "\n");


    }
    fclose(outFile);

    
    
    
    printf("writing cmsh file %s\n", argv[2]);
    outFile = fopen(argv[2], "w");
    color_hash[0] = 0; color_hash[1] = 0; color_hash[2] = 0; color_hash[3] = 0; color_hash[4] = 0;
    for(int e = 0; e < edge_count; e++){
        color_hash[edge_list[e]->color]++;
    }

   fprintf(outFile, "%i\n", tet_count);
   for (int i = 0; i < tet_count; i++)
       fprintf(outFile, "%.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %i %i %i %i\n",
                                                      node_list[tet_list[i]->nodes[0]].coords[0], node_list[tet_list[i]->nodes[0]].coords[1],node_list[tet_list[i]->nodes[0]].coords[2],
                                                      node_list[tet_list[i]->nodes[1]].coords[0], node_list[tet_list[i]->nodes[1]].coords[1],node_list[tet_list[i]->nodes[1]].coords[2],
                                                      node_list[tet_list[i]->nodes[2]].coords[0], node_list[tet_list[i]->nodes[2]].coords[1],node_list[tet_list[i]->nodes[2]].coords[2],
                                                      node_list[tet_list[i]->nodes[3]].coords[0], node_list[tet_list[i]->nodes[3]].coords[1],node_list[tet_list[i]->nodes[3]].coords[2],
                                                      tet_list[i]->sides[0]->pos, 
                                                      tet_list[i]->sides[1]->pos, 
                                                      tet_list[i]->sides[2]->pos, 
                                                      tet_list[i]->sides[3]->pos);

   fprintf(outFile, "%i\n", edge_count);
   for (int i = 0; i < edge_count; i++){
    if( edge_list[i]->re)
        fprintf(outFile, "%.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %i %i %i %i %i %i\n",
                                             node_list[edge_list[i]->nodes[0]].coords[0], node_list[edge_list[i]->nodes[0]].coords[1],node_list[edge_list[i]->nodes[0]].coords[2],
                                             node_list[edge_list[i]->nodes[1]].coords[0], node_list[edge_list[i]->nodes[1]].coords[1],node_list[edge_list[i]->nodes[1]].coords[2],
                                             node_list[edge_list[i]->nodes[2]].coords[0], node_list[edge_list[i]->nodes[2]].coords[1],node_list[edge_list[i]->nodes[2]].coords[2],
                                             edge_list[i]->le->pos, edge_list[i]->re->pos, 
                                             edge_list[i]->lsn, edge_list[i]->rsn, edge_list[i]->orient, edge_list[i]->color -1);
    else
        fprintf(outFile, "%.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %.015lf %i %i %i %i %i %i\n",
                                             node_list[edge_list[i]->nodes[0]].coords[0], node_list[edge_list[i]->nodes[0]].coords[1],node_list[edge_list[i]->nodes[0]].coords[2],
                                             node_list[edge_list[i]->nodes[1]].coords[0], node_list[edge_list[i]->nodes[1]].coords[1],node_list[edge_list[i]->nodes[1]].coords[2],
                                             node_list[edge_list[i]->nodes[2]].coords[0], node_list[edge_list[i]->nodes[2]].coords[1],node_list[edge_list[i]->nodes[2]].coords[2],
                                             edge_list[i]->le->pos, -1, 
                                             edge_list[i]->lsn, edge_list[i]->rsn, edge_list[i]->orient,  edge_list[i]->color -1);
    }
    for(int i = 1; i <=4; i++)
        fprintf(outFile, "%i\n",color_hash[i]);
    fclose(outFile);

    
}
